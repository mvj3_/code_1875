//题目分析:NULL

//题目网址:http://soj.me/1636 

#include <iostream>

using namespace std;

int main()
{
    int testCase;
    int M, N;
    int Ai, Bi;
    int sum = 0;
    cin >> testCase;
    while (testCase--) {
        cin >> M >> N;
        while (N--) {
            cin >> Ai >> Bi;
            sum += Ai * Bi;
        }
        if (sum > M) {
            cout << "Not enough" << endl;
        } else {
            cout << M - sum << endl;
        }
        sum = 0;
    }
    return 0;
}